#include <iostream>
#include <list>

using namespace std;

enum aliasy_ {A, B, C, D, E, F, G, H};

typedef struct Node_ {
    list<int> knowledge;
    list<int>::iterator position;
    int current;
    Node_ *yes, *no;
} Node;

char alias2char(int a) {
    switch (a) {
        case A: return 'a';
        case B: return 'b';
        case C: return 'c';
        case D: return 'd';
        case E: return 'e';
        case F: return 'f';
        case G: return 'g';
        case H: return 'h';
        default: return 'x';  // nieosiągalne przy założeniach programu,
    }
}

void displayNode(Node *node) {
    list<int>::iterator it;
    cout << "\tKnowledge: ";
    for (it = node->knowledge.begin(); it != node->knowledge.end(); ++it) {
        cout << alias2char(*it) << ((it == node->position) ? "* " : "  ");
    }
    cout << endl;
    cout << "\tCurrent: " << alias2char(node->current) << endl;
}

void expand(Node *node, int n) {
    // first we want to see if it's an end node
    if (node->current >= n) return;

    // then we expand yes
    node->yes = new Node;
    node->yes->knowledge = node->knowledge;
    node->yes->position = node->yes->knowledge.begin();
    advance(node->yes->position, distance(node->knowledge.begin(), node->position));
    node->yes->knowledge.insert(node->yes->position, node->current);
    node->yes->position = node->yes->knowledge.begin();
    node->yes->current = node->current + 1;

    // than we expand no (the rules are different)
    node->no = new Node;
    node->no->knowledge = node->knowledge;
    node->no->position = node->no->knowledge.begin();
    advance(node->no->position, distance(node->knowledge.begin(), node->position));
    node->no->position++;
    if (node->no->position == node->no->knowledge.end()) {
        node->no->knowledge.push_back(node->current);
        node->no->current = node->current + 1;
        node->no->position = node->no->knowledge.begin();
    } else {
        node->no->current = node->current;
    }

    // then we run the same for the child nodes
    expand(node->yes, n);
    expand(node->no, n);
}

void nodes2pascal(Node* root, int limit, int depth, bool isElseClause = false) {
    int i;
    list<int>::iterator it;

    for (i = 0; i < depth; ++i) cout << "  ";
    if (isElseClause) cout << "else ";

    // czy jesteśmy w liściu?
    if (root->yes != NULL) {
        // wyświetl warunek
        cout << "if " << alias2char(root->current) << " < " << alias2char(*(root->position)) << " then" << endl;

        // wyświetl podwęzły
        nodes2pascal(root->yes, limit, depth + 1);
        nodes2pascal(root->no, limit, depth, true);
    } else {
        // wyświetl dyrektywę writeln(...)
        if (isElseClause) cout << endl;
        cout << "writeln(";
        for (it = root->knowledge.begin(); it != root->knowledge.end(); ++it) {
            if (it != root->knowledge.begin()) cout << ",";
                cout << alias2char(*it);
        }
        cout << ")" << endl;
    }
}

void tree2pascal(Node* node, int limit, int depth) {
    int i;

    cout << "program sort(input,output);" << endl;
    cout << "var" << endl;
    for (i = 0; i < limit; ++i) {
        if (i != 0) cout << ",";
        cout << alias2char(i);
    }
    cout << " : integer;" << endl;
    cout << "begin" << endl;
    cout << "  readln(";
    for (i = 0; i < limit; ++i) {
        if (i != 0) cout << ",";
        cout << alias2char(i);
    }
    cout << ");" << endl;

    nodes2pascal(node, limit, depth + 1);

    cout << "end." << endl;
}

int main()
{
    int m, n, i;

    cin >> m;
    for (i = 0; i < m; ++i) {
        if (i != 0) cout << endl;
        cin >> n;

        Node root;  // deklaracja w środku dla automatycznego sprzątania
        root.knowledge.push_back(A);
        root.position = root.knowledge.begin();
        root.current = B;

        expand(&root, n);
        tree2pascal(&root, n, 0);
    }

    return 0;
}
